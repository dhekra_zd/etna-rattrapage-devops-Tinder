module.exports = function(app,mongoose,myschema){
	app.get('/user', function(req, res) {

        var userModel = mongoose.model('User', myschema.User);
        userModel.find({}, function(err, users) {
            if (err) {
                throw err
            }
            res.send(users);
        });
    })

    .get('/user/:id', function(req, res) {
        var id = req.params.id;

        var userModel = mongoose.model('User', myschema.User);
        userModel.findById(id, function(err, user) {
            if (err) {
                throw err
            }
            res.status(200).send(user);
        });
    })
    .post('/user', function(req, res) {
        var userModel = mongoose.model('User', myschema.User);
		var id = req.body.id;
		var firstName = req.body.firstName;
        var lastName =	req.body.lastName;
        var email = req.body.email;
		
		if (id.length == 0 || firstName.length == 0 || lastName.length == 0 || email.length == 0){
		res.status(400);
		res.send("invalid input, object invalid");
		next();
		}
        var user = new userModel({
            _id: req.body.id,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email
        });
        userModel.find({
                id: req.body.id
            }, function(err, users) {
            if (!users.length) {
				user.created_date = new Date();
                user.save(function(err) {
                    if (err) {
                        throw err;
                        res.status(400);
                        res.send("invalid input, object invalid");
						return;
                    }
					else {  
						res.status(201);
						res.send("Item created");
					}
                });
            } else {
			if(!res.headersSent)
              res.status(409).send("an existing item already exists");
            }
			return;
        });
		
    })

    .put('/user/:id', function(req, res) {
        var userModel = mongoose.model('User', myschema.User);

		var id = req.params.id;
		var firstName = req.body.firstName;
        var lastName =	req.body.lastName;
        var email = req.body.email;
		
		if (id.length == 0 || firstName.length == 0 || lastName.length == 0 || email.length == 0){
		res.status(400);
		res.send("invalid input, object invalid");
		next();
		}
		
        userModel.findById(id, function(err, user) {
            if (user == null) {
                res.status(400);
                res.send("no item with id:" + id);
            } else {
                user.firstName = req.body.firstName;
                user.lastName = req.body.lastName;
                user.email = req.body.email;
                user.save(function(err, updateuser) {
                    if (err) {
                        res.status(400);
                        res.send("invalid input, object invalid");
                    }
					res.status(201);
					res.send("Item Created");
                });
            }
        });

    });
}