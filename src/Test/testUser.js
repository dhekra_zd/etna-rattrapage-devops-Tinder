var http = require('http');
var assert = require('assert') 
var id = "testtest";
module.exports = {

	getUser : function test1(){
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/user'
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});

			response.on('end', function() {
				var result = JSON.parse(data);
				if (result.length < 1){
					console.log("Get All users Failed !" );
					process.exit()
				}
				console.log("Get All users Success !" );
			});
		});
		req.on('error', function(err) {
			console.log("Get users Failed !" );
			process.exit()
		});

		req.end();
	},
	getUserId : function test1(){
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/user/' + id
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});

			response.on('end', function() {
				var result = JSON.parse(data);
				if (result.length < 1){
					console.log("Get user Failed !" );
					process.exit()
				}
				console.log("Get user Success !" );
				module.exports.putUser();
			});
		});
		req.on('error', function(err) {
			console.log("Get user Failed !" );
			process.exit()
		});

		req.end();
	},
	postUser : function test1(mongoose,messageModel,func){
		messageModel.findById(id).remove().exec();
		var jsonObject = JSON.stringify({
			  id: id,
			  firstName: "Anthony",
			  lastName: "Hopkins",
			  email: "anthony.hopkins@club-internet.fr",
			  created_date: ""
		});
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/user',
		    method: "POST",
			    headers: {
				"Content-Type": "application/json",
				"Content-Length": Buffer.byteLength(jsonObject)
				},
			body: jsonObject
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on('end', function() {
				assert.deepEqual(response.statusCode, 201, "Create user Failed (Status code) ! ");
				console.log("Create user Success !" );
				func();
			});
		});
		req.on('error', function(err) {
			console.log("Create user Failed !" );
			process.exit()
		});
		req.end(jsonObject);
	},
	putUser : function test1(){
		var jsonObject = JSON.stringify({
			  id: id,
			  firstName: "Anthonys",
			  lastName: "Hopkins",
			  email: "anthony.hopkins@club-internet.fr",
			  created_date: ""
		});
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/user/'+id,
		    method: "PUT",
			    headers: {
				"Content-Type": "application/json",
				"Content-Length": Buffer.byteLength(jsonObject)
				},
			body: jsonObject
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on('end', function() {
				console.log(data);
				assert.deepEqual(response.statusCode, 201, "Update user Failed (Status code) ! ");
				console.log("Update user Success !" );
				module.exports.getUser();
			});
		});
		req.on('error', function(err) {
			console.log("Update user Failed !" );
			process.exit()
		});
		req.end(jsonObject);
	}

	
}