var http = require('http');
var assert = require('assert') 
var id = "testtest";
module.exports = {

	getInbox : function test1(){
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/inbox'
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});

			response.on('end', function() {
				var result = JSON.parse(data);
				if (result.length < 1){
					console.log("Get All inbox Failed !" );
					process.exit()
				}
				console.log("Get All inbox Success !" );
			});
		});
		req.on('error', function(err) {
			console.log("Get inbox Failed !" );
			process.exit()
		});

		req.end();
	},
	getInboxId : function test1(){
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/inbox/' + id
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});

			response.on('end', function() {
				var result = JSON.parse(data);
				if (result.length < 1){
					console.log("Get inbox Failed !" );
					process.exit()
				}
				console.log("Get inboxId Success !" );
				module.exports.getInbox();
			});
		});
		req.on('error', function(err) {
			console.log("Get inbox Failed !" );
			process.exit()
		});

		req.end();
	},
	postInbox : function test1(mongoose,messageModel,func){
		messageModel.findById(id).remove().exec();
		var jsonObject = JSON.stringify({
			 id : "testtest",
			from : "53467235483cb56c475cc1d6",
			to : "533a59ea52046fc077002815",
			match_id : id,
			sent_date : {},
			created_date : {},
			content : "Hello !"
		});
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/inbox',
		    method: "POST",
			    headers: {
				"Content-Type": "application/json",
				"Content-Length": Buffer.byteLength(jsonObject)
				},
			body: jsonObject
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on('end', function() {
				assert.deepEqual(response.statusCode, 201, "Create inbox Failed (Status code) ! ");
				console.log("Create inbox Success !" );
				func();
			});
		});
		req.on('error', function(err) {
			console.log("Create inbox Failed !" );
			process.exit()
		});
		req.end(jsonObject);
	}

	
}